# API Gateway



## Description
The API Gateway is a microservice responsible for routing and managing requests to various backend microservices in the 
application. It acts as a single entry point for clients and provides a unified interface to access different 
functionalities offered by the system.

## Features
- [ ] Routing: The API Gateway handles request routing to different microservices based on predefined rules and 
configurations.

## Technologies Used
- [ ] Spring Boot
- [ ] Spring Cloud Gateway

## Installation/Usage
The API Gateway is part of a larger, complex architecture. To run it, please refer to the 
[docker-compose](https://gitlab.com/grzegorz-oladele/docker-compose)repository, where detailed instructions on running
the entire architecture are provided.

## Custom Pipeline
This repository includes a custom pipeline that automates the build, testing, and deployment process for the application
The pipeline is designed to perform the following steps:

1. Build and test:
    - [ ] The pipeline triggers a build process using Maven to compile and package the application.
    - [ ] It runs the tests to ensure the code quality and functionality.
    - [ ] If the build and tests pass successfully, the pipeline proceeds to the next step.
    - [ ] In case the testing process fails, the pipeline terminates, and the test logs are saved in the newly created
      **errors.txt** file

2. Commit and push:
    - [ ] The pipeline commits and pushes the changes to the Git repository on GitLab.
    - [ ] It includes the commit message provided as an argument when running the pipeline.

3. Docker image build and push:
    - [ ] The pipeline builds a Docker image of the application.
    - [ ] It tags the Docker image with the first 6 characters of the current commit's hash.
    - [ ] If the optional -d (docker) argument is present when running the pipeline, it automatically pushes the Docker
      image to the Docker Hub repository.

***Example command execution:*** `gateway-build -cm"commit message" -d`

Before each pipeline execution, the script checks the existence of the pipeline folder and removes it if it already
exists. This ensures a clean environment for the pipeline to run successfully.

Please note that the pipeline assumes the necessary dependencies, such as Maven, Git, and Docker, are properly
installed and configured on the build environment.

# Project Status
The motorcycle-microservice is actively being developed. We are continuously working on adding new features and 
improving the project.

# Author
**Grzegorz Oladele**

Thank you for your interest in the api-gateway project! If you have any questions or feedback, please
feel free to contact us.