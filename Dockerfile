FROM amazoncorretto:17-alpine
COPY target/api-gateway-0.0.1-SNAPSHOT.jar api-gateway-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "api-gateway-0.0.1-SNAPSHOT.jar"]